package com.exmaple.myjavalearning.lesson3;

import java.util.ConcurrentModificationException;

public class DynamicArray {
    private final int DEFAULT_SIZE = 10;
    private int count = 0;
    private Object[] elements;
    private int modCounter = 0;

    //Конструктор 1
    public DynamicArray() {
        elements = new Object[DEFAULT_SIZE];
    }

    //Конструктор 2
    public DynamicArray(int size) {
        elements = new Object[size];
    }

    //Валидатор индекса
    private void validateIndex(int i) {
        if (i > count - 1 || i < 0)
            throw new ArrayIndexOutOfBoundsException();
    }

    //Логирование
    private void log(String s) {
        System.out.println(">> " + s);
    }

    //Увеличть массив в 1,5 раза (нарастить справа)
    private void expand() {
        Object[] newElements = null;
        int newLength = (int) ((elements.length + 1) * 1.5);
        newElements = new Object[newLength];
        System.arraycopy(elements, 0, newElements, 0, elements.length);

        elements = newElements;
    }

    //Сузить массив в 1,5 раза (убрать лишние ненужные ячейки справа)
    private void shrink() {
        Object[] newElements = null;
        int newLength = (int) ((elements.length + 1) / 1.5);
        newElements = new Object[newLength];
        System.arraycopy(elements, 0, newElements, 0, newLength);

        elements = newElements;
    }

    //Все элементы, начиная с fromPosition включительно, сдвинуть вправо на 1
    private void shiftRight(int fromPosition) {
        //Убедиться, что сдвиг внутри логического массива
        validateIndex(fromPosition);

        //Если физически не хватит места для сдвига, нарастить массив
        if (elements.length == count) {
            expand();
        }

        //Сдвинуть
        System.arraycopy(elements, fromPosition, elements, fromPosition + 1, elements.length - fromPosition - 1);
        elements[fromPosition] = null;

        //Увеличить счетчик, т.к. логическая длина массива увеличивается на 1
        count++;
        //Увеличить версию
        modCounter++;
    }

    //Сдвиг влево с позиции - удаляется элемент в позиции fromPostition
    private void shiftLeft(int fromPosition) {
        //Убедиться, что сдвиг внутри логического массива
        validateIndex(fromPosition);

        //Сузить, если больше чем в 1.5 раза больше
        if (1.5 < (float) elements.length / count)
            shrink();

        System.arraycopy(elements, fromPosition + 1, elements, fromPosition, count - fromPosition - 1);
        elements[--count] = null;
        modCounter++;
    }

    //Добавить элемент в конец массива
    public boolean add(Object o) {
        //Нарастить массив, если достигнут предел
        if (elements.length == count)
            expand();

        elements[count++] = o;
        modCounter++;

        return true;
    }

    //Добавить после указанного элемента
    public boolean add(int i, Object o) {
        if (count == 0) {
            add(o);
        } else {
            //Сдвинуть вправо (с увеличением счетчика)
            shiftRight(i);
            //Присвоить элемент в позицию
            set(o, i);
        }

        return true;
    }

    //Удалить первый найденный объект
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index > -1) {
            shiftLeft(index);
            return true;
        }
        return false;
    }

    //Удалить по индексу
    public boolean remove(int i) {
        shiftLeft(i);
        return true;
    }

    //Присвоить элемент по индексу
    public void set(Object o, int i) {
        validateIndex(i);
        elements[i] = o;
        modCounter++;
    }

    //Индекс объекта
    public int indexOf(Object o) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    //Получить элемент
    public Object get(int i) {
        validateIndex(i);
        return elements[i];
    }

    //Размер
    public int size() {
        return count;
    }

    //Содержит ли
    public boolean Contains(Object o) {
        return (indexOf(o) > -1);
    }

    //В массив
    public Object[] toArray() {
        return elements;
    }

    //Напечатать массив
    public void print() {
        System.out.println("Elements:");
        for (int i = 0; i < count; i++)
            System.out.println(i + ": " + elements[i]);
        System.out.println("Elements count: " + count);
        System.out.println("Physical size: " + elements.length);
    }

    public class ListIterator {

        int pos;
        public int modCounter;

        //Актуализатор счетчика изменений итератора
        private void checkModificationNumber() {
            if (modCounter != DynamicArray.this.modCounter) {
                modCounter = DynamicArray.this.modCounter;
                throw new ConcurrentModificationException();
            }
        }

        //Конструктор
        public ListIterator() {
            modCounter = DynamicArray.this.modCounter;
            pos = 0;
        }

        //Проверить, есть ли следующий элемент
        public boolean hasNext() {
            checkModificationNumber();
            return (pos + 1) <= DynamicArray.this.size() - 1;
        }

        //Вернуть следующий элемент
        public Object next() {
            Object o;

            checkModificationNumber();
            o = DynamicArray.this.get(pos + 1);
            pos++;
            return o;
        }

        //Проверить, есть ли предыдущий элемент
        public boolean hasPrevious() {
            checkModificationNumber();
            return pos > 0;
        }

        //Вернуть предыдущий элемент
        public Object previous() {
            Object o;

            checkModificationNumber();
            o = DynamicArray.this.get(pos - 1);
            pos--;
            return o;
        }

        //Следующий индекс
        public int nextIndex() {
            checkModificationNumber();
            if (hasNext()) {
                return pos + 1;
            }
            else {
                return DynamicArray.this.size();    //Если это последний элемент, вернуть размер списка (подсмотрел в описании у ListIterator)
            }
        }

        //предыдущий индекс
        public int previuosIndex() {
            checkModificationNumber();
            return pos - 1;
        }

        //Удалить текущий элемент
        public void remove() {
            checkModificationNumber();
            DynamicArray.this.remove(pos);
            modCounter = DynamicArray.this.modCounter;
        }

        //Задать текущий эелемент
        public void set(Object o) {
            checkModificationNumber();
            DynamicArray.this.set(o, pos);
            modCounter = DynamicArray.this.modCounter;
        }

        //Добавить эелемент в текущую позицию (со сдвигом текущего элемента и всех последующих вправо)
        public void add(Object o) {
            checkModificationNumber();
            DynamicArray.this.add(pos, o);
            modCounter = DynamicArray.this.modCounter;
        }

    }

}
