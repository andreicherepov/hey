package com.exmaple.myjavalearning.lesson3;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class IteratorTest {

    private DynamicArray getArrayOf2()
    {
        int size = 2;
        DynamicArray da = new DynamicArray(size);
        for (int i = 0; i < size; i++)
            da.add(i + " element");
        return da;
    }

    @Test
    public void hasNextTest()
    {
        DynamicArray da = getArrayOf2();
        DynamicArray.ListIterator i1 = da.new ListIterator();
        DynamicArray.ListIterator i2 = da.new ListIterator();

        assertTrue(i1.hasNext());
        i1.next();
        assertFalse(i1.hasNext());

        try
        {
            i1.set("new");
            i2.hasNext();
            Assert.fail();
        }
        catch (Exception e)
        {
            assertEquals(e.toString(), "java.util.ConcurrentModificationException");
        }
    }
}

