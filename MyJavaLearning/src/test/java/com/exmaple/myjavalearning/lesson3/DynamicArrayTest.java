package com.exmaple.myjavalearning.lesson3;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class DynamicArrayTest {

    @Test
    public void addInBegining() {
        DynamicArray da = new DynamicArray();
        da.add("Str0");

        int addPos = 0;
        String addObj = "Str";
        assertTrue(da.add(addPos, addObj));
        assertEquals(da.get(addPos), addObj);
    }

    @Test
    public void removeFromBegining() {
        DynamicArray da = new DynamicArray();
        int removePos = 0;
        String removeObj = "RemoveStr";
        String remainObj = "RemainStr";

        da.add(removeObj);
        da.add(remainObj);

        assertTrue(da.remove(removePos));
        assertEquals(da.get(removePos), remainObj);
    }

    @Test
    public void addInMiddle() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add(i);

        int size = da.size();
        int addPos = 5;
        String addObj = "Str";

        assertTrue(da.add(addPos, addObj));
        assertEquals(da.size(), size + 1);
        assertEquals(da.get(addPos), addObj);
    }

    @Test
    public void removeFromMiddle() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add("Str" + i);

        int size = da.size();
        int removePos = 5;
        String removeObj = (String) da.get(removePos);

        assertTrue(da.remove(removePos));
        assertEquals(da.size(), size - 1);
        assertFalse(da.Contains(removeObj));
    }

    @Test
    public void addInEnd() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add(i);

        int size = da.size();
        int addPos = da.size() - 1;
        String addObj = "Str";

        assertTrue(da.add(addPos, addObj));
        assertEquals(da.size(), size + 1);
        assertEquals(da.get(addPos), addObj);
    }

    @Test
    public void removeFromEnd() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add("Str" + i);

        int size = da.size();
        String removeObj = (String) da.get(da.size() - 1);

        assertTrue(da.remove(da.size() - 1));
        assertEquals(da.size(), size - 1);
        assertNotEquals(da.get(da.size() - 1), removeObj);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void addOutOfBounds() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add("Str" + i);

        thrown.expect(ArrayIndexOutOfBoundsException.class);
        da.add(da.size() + 1, "Str");
    }

    @Test
    public void removeOutOfBounds() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++)
            da.add("Str" + i);

        thrown.expect(ArrayIndexOutOfBoundsException.class);
        da.remove(da.size() + 1);
    }

    @Test
    public void DynamicArrayPerformanceTest() {
        long[] orig = new long[3];
        long[] my = new long[3];

        //Оригинал ------------------------
        //Старт
        orig[0] = System.currentTimeMillis();

        //Создание
        ArrayList<String> a = new ArrayList<String>();

        //заполнение (с вставкой в центре)
        for (int i = 0; i < 10; i++)
            a.add("Str");
        for (int i = 0; i < 100000; i++)
            a.add(8, "Str" + i);

        orig[1] = System.currentTimeMillis();

        //Удаление
        for (int i = 0; i < 50000; i++)
            a.remove(7);

        orig[2] = System.currentTimeMillis();

        //Мой ------------------------
        //Старт
        my[0] = System.currentTimeMillis();

        //Создание
        DynamicArray a2 = new DynamicArray();

        //заполнение (с вставкой в центре)
        for (int i = 0; i < 10; i++)
            a2.add("Str");
        for (int i = 0; i < 100000; i++)
            a2.add(7, "Str" + i);

        my[1] = System.currentTimeMillis();

        //Удаление
        for (int i = 0; i < 50000; i++)
            a2.remove(8);

        my[2] = System.currentTimeMillis();

        //Разница
        String addLogStr = "Заполнение в милисекундах (ArrayList - DynamicArray): " + (orig[1] - orig[0]) + " - " + (my[1] - my[0]);
        String remLogStr = "Удаление в милисекундах (ArrayList - DynamicArray): " + (orig[2] - orig[1]) + " - " + (my[2] - my[1]);
        System.out.println(addLogStr);
        System.out.println(remLogStr);

        try (FileWriter writer = new FileWriter("performance.txt", false)) {
            writer.write(addLogStr + "\n");
            writer.write(remLogStr);


            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
